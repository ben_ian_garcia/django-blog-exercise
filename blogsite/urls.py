from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from blog import views

app_name = 'blog'
urlpatterns = [
    path('', auth_views.LoginView.as_view(template_name='blog/index.html',redirect_authenticated_user=True), name='signin'),
    path('signout', auth_views.LogoutView.as_view(template_name='blog/signout.html'), name='signout'),
    path('signup/', views.signup, name='signup'),
    path('welcome/', views.welcome, name='welcome'),
    path('admin/', admin.site.urls),
]
