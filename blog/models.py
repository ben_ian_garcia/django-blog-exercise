from django.db import models
from django.forms import ModelForm
from django import forms

# Create your models here.
class User(models.Model):
    username = models.CharField(max_length=20, unique=True, default="" )
    password = models.CharField(max_length=20)
    email = models.EmailField(max_length=254, unique=True, default="")
    firstname = models.CharField(max_length=30, default="")
    lastname = models.CharField(max_length=30, default="")

"""
class UserForm(ModelForm):
    class Meta:
        model = User
        widgets = {'password': forms.PasswordInput(),}
        fields = ['username', 'password', 'email', 'firstname', 'lastname']

class SignInForm(ModelForm):
    class Meta:
        model = User
        widgets = {'password': forms.PasswordInput(),}
        fields = ['username', 'password']
"""