from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib import messages

#from .forms import SignUpForm
from .models import User
from .forms import UserSignUpForm

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        return render(request, "blog/welcome.html")
    else:
        return render(request, "blog/index.html")

def signup(request):
    if request.method == 'POST':
        form = UserSignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            form.save()
            messages.success(request, f'Account successfully created for { username }')
            return redirect('signin')
    else:
        form = UserSignUpForm()
    return render(request, "blog/signup.html",{
        "form": form,
    })

@login_required
def welcome(request):
    return render(request, "blog/welcome.html")

"""
def index(request): 
    message_line = ''
    username = request.POST.get('username')
    password = request.POST.get('password')
    uname = str(username)
    if request.method == 'POST':
        try:
            obj = User.objects.get(username=uname)
            un = obj.username
            pw = obj.password
            firstname= obj.firstname
            lastname = obj.lastname
            if password != pw:
                raise Exception
            else:
                return render(request, 'blog/welcome.html', {
                    'username':un,
                    'password':pw,
                    'firstname':firstname,
                    'lastname': lastname,
                })
        except:
            if username == "None":
                message_line = " "
            else:
                message_line = "Login failed: Invalid username or password"
        return render(request, 'blog/index.html', {
            'username': username,
            'password': password,
            'message_line': message_line,
        })

    return render(request, 'blog/index.html', {
        'message_line': message_line,
    })

    if request.method == 'POST':
        form = SignInForm(request.POST)
        #if form.is_valid():
         #   return HttpResponseRedirect('blog/welcome.html')            
    else:
        form = SignInForm()
    return render(request, 'blog/index.html', {
        'form': form,
    })
    #return HttpResponse("This  is a Sign up Page")

def signup(request):
    error_message = ""
    signup_okay = ""
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            signup_okay = "Sign Up Successful!"
        else:
            error_message = "Please fill out the form properly"
    else:
        form = UserForm()
    return render(request, 'blog/signup.html', {
        'form': form,
        'error_message': error_message,
        'signup_okay' : signup_okay,
    })

def welcome(request):
    return HttpResponse("Welcome User!")   

"""