from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class UserSignUpForm(UserCreationForm):
    email = forms.EmailField()
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'email', 'first_name' , 'last_name']
"""
class SignUpForm(forms.Form):
    user_name = forms.CharField(label="Username", max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField(label="Email", max_length=50)
    firstname = forms.CharField(label="First Name", max_length=50)
    lastname = forms.CharField(label="Last Name", max_length=50)
"""