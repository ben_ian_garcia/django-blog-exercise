from django.contrib import admin

# Register your models here.
from .models import User

class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Account Information', {'fields': ['username', 'password', 'email']}),
        ('Personal Details', {'fields': ['firstname', 'lastname']})
    ]
    list_display = ('username', 'firstname', 'lastname', 'email')

admin.site.register(User, UserAdmin)